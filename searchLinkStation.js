/**
 * Calculate distance between two points.
 * @param {Array} pointA - location of a device.
 * @param {Array} pointB - location of a link station.
 * @returns {number}
 */
getDistanceBetweenPoints = (pointA, pointB) => {
    const [x1, y1] = pointA;
    const [x2, y2] = pointB;
    return Math.sqrt(Math.abs(x1 - x2) ** 2 + Math.abs(y1 - y2) ** 2);
}

/**
 * Calculate power of a link station based on a given formula.
 * power = (reach - distance)^2
 * @param {number} reach - reach of link station power.
 * @param {number} distance - between a device and link station.
 * @returns {number}
 */
getLinkStationPower = (reach, distance) => {
    return reach > distance ? parseFloat(((reach - distance) ** 2).toFixed(2)) : 0;
}

/**
 * Find a link station with the most power for a device at location/point x and y.
 * @param {Array} device - location of a device.
 * @param {Array} stations - link stations.
 * @returns {Object}
 */
getStationWithMostPower = (device, stations) => {
    return stations
    .map((station) => {
        const distance = getDistanceBetweenPoints(device, station.location);
        const power = getLinkStationPower(station.reach, distance);
        return {
            point: device,
                linkStation: station.location,
                power: power,
            };
        })
        .sort((a, b) => (a.power > b.power ? 1 : -1))
        .pop();
}

/**
 * Get status if link station with most power for a point is found or not.
 * @param {Array} devices - locations of devices.
 * @param {Array} stations - list stations.
 * @returns {Array} - search status text
 */
getStationSearchStatus = (devices, stations) => {
    return devices.map((device) => {
        const { point, linkStation, power } = getStationWithMostPower(device, stations);
        let status = `No link station within reach for point ${point}`;
        status = power ? `Best link station for point ${point} is ${linkStation} with power ${power}` : status; 
        return status;
    });
}

/**
 * Format link station data for further usage.
 * @param {Array} stationData - link station location and reach.
 * @returns {Object}
 */
formatLinkStationData = (stationData) => {
    return stationData.map((data) => {
        const [x, y, reach] = data;
        return {
            location: [x, y],
            reach: reach,
            power: 0
        };
    });
}

/**
 * Entry point with link station and device location test data.
 */
init = () => {
    const devices = [
        [0, 0],
        [100, 100],
        [15, 10],
        [18, 18]
    ];
    const stationData = [
        [0, 0, 10],
        [20, 20, 5],
        [10, 0, 12]
    ];
    const stations = formatLinkStationData(stationData);
    getStationSearchStatus(devices, stations).forEach(status => console.log(status));
}

init();
