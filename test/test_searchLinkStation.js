const { expect } = require('chai');
const rewire = require('rewire');
const app = rewire('../searchLinkStation.js');

const getDistanceBetweenPoints = app.__get__('getDistanceBetweenPoints');
describe('#getDistanceBetweenPoints()', () => {

    it('should calculate distance between two points and return a number', () => {
        const point1 = [0, 0];
        const point2 = [100, 100];

        const distance = getDistanceBetweenPoints(point1, point2);
        expect(distance).to.be.equal(141.4213562373095);
    });
});

describe('validate getLinkStationPower()', () => {
    const getLinkStationPower = app.__get__('getLinkStationPower');
    it('should calculate power based on reach and distance', () => {
        const reach = 89.5;
        const distance = getDistanceBetweenPoints([4, 10], [42, 90]);
        const power = getLinkStationPower(reach, distance);
        expect(power).to.be.equal(0.87);
    });

    it('power should return 0 if reach is lessthan distance', () => {
        const reach = 85.56;
        const distance = getDistanceBetweenPoints([4, 10], [42, 90]);
        const power = getLinkStationPower(reach, distance);
        expect(power).to.be.equal(0);
    });

    it('power should return 0 if reach is equal to distance', () => {
        const reach = 85.57;
        const distance = getDistanceBetweenPoints([4, 10], [42, 90]);
        const power = getLinkStationPower(reach, distance);
        expect(power).to.be.equal(0);
    });
});

describe('validate logStationSearchStatus()', () => {
    const getStationSearchStatus = app.__get__('getStationSearchStatus');
    const formatLinkStationData = app.__get__('formatLinkStationData');
    const points = [
        [0, 0],
        [50, 45]
    ];
    const stationData = [
        [0, 0, 20],
        [11, 13, 7],
        [1, 0, 12]
    ];
    const station = formatLinkStationData(stationData);
    it('should find a station with the most power for a device if within reach', () => {
        const searchStatus = getStationSearchStatus(points, station);
        expect(searchStatus[0]).to.be.equal('Best link station for point 0,0 is 0,0 with power 400');
    });

    it('should not find a station if not within reach of a device location', () => {
        const searchStatus = getStationSearchStatus(points, station);
        expect(searchStatus[1]).to.be.equal('No link station within reach for point 50,45');
    });
});
